# Reference Documentation

Welcome to the `reference` directory! This area collects factual, data-driven, and specific information about our service. If you're seeking tables, lists, technical specs, or any definitive details, you're in the right place.

## Contents

- [`service-sla.md`](service-sla.md) - The Service Level Agreement details for our service, outlining our commitments to uptime, response times, and more.
- [`recurrent-incidents.md`](recurrent-incidents.md) - A list and analysis of recurring incidents, aiding in troubleshooting and prevention.
... And more!

Remember, while reference documentation is valuable for specifics, if you're looking for actionable steps or conceptual explanations, you might want to explore the `procedure` or `SDP` directories respectively.
