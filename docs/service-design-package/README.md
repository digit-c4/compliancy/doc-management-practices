# Service Design Package Documentation (SDP)

Welcome to the `SDP` directory! Here, you'll find all the documents a thorough understanding of the design, principles, and foundational knowledge related to this service.

## Contents

- **[ADR (Architectural Decision Records)](ADR.md)**: Understand the design decisions made during the project's lifecycle.
- **[System Description](system-description.md)**: Get an overview of the system, its components, and its purpose.
- **[System Requirements](system-requirements.md)**: Discover the prerequisites and requirements for our system.
- **[Technical Architecture](technical-architecture.md)**: Dive into the structural design of our system.
- **[Security](security.md)**: Understand the security measures and protocols in place.
- **[Capacity & Performance](capacity-performance.md)**: Learn about the system's performance metrics and capacity considerations.
- **[Transition Plan](transition-plan.md)**: Know how the system will transition from the design phase to operations.
- **[Dependencies](dependencies.md)**: Understand the external systems or components our service depends on.

### Additional Resources

- **[External Documentation](external-doc/)**: Access external documents related to our service.
- **[Images](images/)**: Visual aids and diagrams that provide a graphical representation of our concepts.

## Contributing

If you wish to contribute to this documentation, please refer to the main [CONTRIBUTING.md](../CONTRIBUTING.md) guide in the root of the repository.

## Feedback

Your feedback is valuable! If you have suggestions or find any errors, please create an issue in the main repository or contact the PPO of the squad.
