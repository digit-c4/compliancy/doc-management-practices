# Document Title

## Introduction

The introduction to a design module is a single, concise paragraph that provides a short overview of the module. A short description makes the module more usable because users can quickly determine whether the concept is useful without having to read the entire module.

The introduction typically answers the following questions:

- What is the concept?
- Why should the user care about the concept?

## Concept Explanation

Paragraph 1 content here.

Paragraph 2 content here.

Paragraph 3 content here.

Apart from paragraphs, you can use other elements, such as lists, tables, or examples. Consider including graphics or diagrams to speed up the understanding of the concept.

> **IMPORTANT**
> 
> Avoid including instructions to perform an action. Action items belong in procedure modules. However, in some cases, a concept or reference module can include suggested actions when those actions are simple, highly dependent on the context of the module, and have no place in any procedure module.

## Additional Resources

- [Link Text 1](url)
- [Link Text 2](url)
- [Link Text 3](url)

The optional additional resources list links to other material closely related to the contents of the concept module, for example, other documentation resources.
