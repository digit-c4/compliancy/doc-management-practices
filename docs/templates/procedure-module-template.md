## Introduction
The procedure introduction is a short paragraph that provides an overview of the module. The introduction includes what the module will help the user do and why it will be beneficial to the user.

The introduction typically provides context for the procedure, such as:

- Why and where the user performs the procedure
- Special considerations specific to the procedure

## Prerequisites
This section is optional. Prerequisites are a bulleted list of conditions that must be satisfied before the user starts the procedure. Use a bulleted list for prerequisites and the plural heading Prerequisites, even if you only have a single prerequisite.
Focus on relevant prerequisites that users might not otherwise be aware of. Do not list obvious prerequisites. 

## Procedure Steps 
The procedure consists of one or more steps required to complete the procedure. Each step describes one action. For single-step procedures, use an unnumbered bullet instead of a numbered list.

## Procedure Verification (optional)
It provides the user with one or more steps to verify that the procedure provided the intended outcome, for example:

- An example of expected command output or a pop-up window that the user receives when the procedure is successful
- Actions for the user to complete, such as entering a command, to determine the success or failure of the procedure

## Additional Resources
This section is optional. The additional resources list links to other material closely related to the contents of the procedure module, such as other documentation resources, instructional videos, or labs.

