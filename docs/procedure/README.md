# Procedure Documentation

Welcome to the `procedure` directory! Here, you'll find step-by-step guides and actionable documentation related to the various operations and tasks associated with our service.

## Contents

- [`deployment.md`](deployment.md) - A guide on how to deploy the service.
- [`release-testing.md`](release-testing.md) - Steps and practices for testing new releases.
- [`service-checklist.md`](service-checklist.md) - A checklist to ensure our service runs smoothly.
- [`it-service-continuity.md`](it-service-continuity.md) - Procedures to ensure the continuous operation of our service.
- [`capacity-availability-management.md`](capacity-availability-management.md) - Guidelines for managing capacity and availability.
- [`vulnerability-management.md`](vulnerability-management.md) - Steps to identify and address vulnerabilities.
- [`logging-monitoring.md`](logging-monitoring.md) - Guidelines on logging practices and monitoring the service.
- [`event-management.md`](event-management.md) - How to manage and respond to various events.
- [`request-fulfilment.md`](request-fulfilment-L1.md) - Procedures to fulfill service requests at Level 1.
- [`incident-troubleshooting.md`](incident-troubleshooting-L1.md) - Troubleshooting guidelines for incidents.
- ... And more!

Feel free to dive into each document for detailed insights and instructions. If you're unsure where to start, consider the nature of your task or query and select the most relevant guide.
