# Documentation Management Best Practices

This **PUBLIC** project focuses on establishing document management best practices for all our services.

## What is the Purpose of these Best Practices?

The purpose is to organise our service documentation in a standardized way and make it easily accessible and maintainable. This project also introduces readers to modular documentation. 

## What is Modular Documentation
 
Modular documentation is built on the principle of smaller, independent modules that can be written, edited, and reused across various documents. Each module focuses on a single topic or concept, ensuring clarity and minimizing redundancy.
Each module is designed to stand alone, offering meaningful insight and serving a purpose even when separated from its counterparts.
The repository structure and the template files included in this project will help you better understanding this concept.

## Why Are We Doing This?

To assist the squads in enhancing their documentation agility and ensure that documents remain current and are easily discoverable by others. Our focus is on catering to the organization's needs, including operations and end-users, rather than merely emphasizing feature-centric documentation. 

## Repository Structure

This repository uses a specific directory structure, which is recommended for all service repositories to maintain consistency. Here's an overview of this structure:

```
📦service -repo/
├── docs/
│   ├── service-design-package/ (delves into the design and purpose of the service - FOR UNDERSTANDING)
│   │   ├── README.md
│   │   ├── ADR.md
│   │   ├── system-description.md
│   │   ├── technical-architecture.md
│   │   ├── security.md
│   │   ├── capacity-performance.md
│   │   ├── transition-plan.md
│   │   ├── dependencies.md
│   │   ├── ...
│   │   ├── external-doc/
│   │   ├── images/
│   ├── procedure/ (guides on actionable steps for the operations - HANDS-ON ACTION)
│   │   ├── README.md
│   │   ├── deployment.md
│   │   ├── release-testing.md
│   │   ├── service-checklist.md
│   │   ├── it-service-continuity.md
│   │   ├── vulnerability-management.md
│   │   ├── logging-monitoring.md
│   │   ├── event-management.md
│   │   ├── request-fulfilment-L1.md
│   │   ├── incident-troubleshooting-L1.md
│   │   ├── ...
│   ├── reference/ (provides factual and reference information such as tables, lists, or technical specs)
│   │   ├── README.md
│   │   ├── service-sla.md
│   │   ├── recurrent-incidents
│   ├── templates
│   │   ├── design-module-template-expanded.md
│   │   ├── design-module-template-short.md
│   │   ├── procedure-module-template.md
│   │   ├── procedure-module-template.adoc
│   │   ├── reference-module-template.md
├── README.md (this file)
├── CONTRIBUTING.md (how-to contribute to this project)
├── CHANGELOG.md (history of notable changes made to the project in each release)
```

## Service Documentation Template Files

Another deliverable for this project is the templates for the various document types. All the template files for writing new modular content can be found [here](./docs/templates/)


## Feedback & Collaboration

We are in the process of establishing our best practices, and during this critical phase, your input is invaluable. If you have suggestions, questions, or encounter any issues, please raise them in the Issues section.
For detailed guidance on how to contribute, please see our [Contributing Guidelines](CONTRIBUTING.md). Your participation is key to the success of this project.

