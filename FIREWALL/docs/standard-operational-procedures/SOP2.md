```mermaid
flowchart LR
    A(request) --> B(acknowledge<br/>request)
    B --> C(implement)
    C --> D(push)
    D -->|issue| B
    D -->|OK| F(close)
```

